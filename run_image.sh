#!/usr/bin/env bash

set -e

IMAGE_NAME="mylnz/market"

docker pull "$IMAGE_NAME":latest
docker run -e "SPRING_PROFILES_ACTIVE=development" -p 8080:8080 -d "$IMAGE_NAME"
