package com.gamesys.market.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gamesys.market.dto.MarketDataDto;
import com.gamesys.market.model.MarketData;
import com.gamesys.market.service.MarketDataService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@WebMvcTest(value = MarketDataController.class)
class MarketDataControllerTest {

    @MockBean
    MarketDataService marketDataService;

    @MockBean
    ModelMapper modelMapper;

    @InjectMocks
    MarketDataController marketDataController;

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        when(modelMapper.map(ArgumentMatchers.any(), ArgumentMatchers.any())).thenReturn(toDto(getMockMarketData()));
    }


    @Test
    void findById() throws Exception {

        MarketData mockData = getMockMarketData();

        when(marketDataService.findById(mockData.getId())).thenReturn(Optional.of(mockData));

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get("/v1/market-data/" + mockData.getId())
                .accept(MediaType.APPLICATION_JSON);

        MockHttpServletResponse response = mockMvc.perform(requestBuilder).andReturn().getResponse();

        assertEquals(HttpStatus.OK.value(), response.getStatus());
    }

    @Test
    void findAll() throws Exception {
        List<MarketData> marketDataList = new ArrayList<>();

        marketDataList.add(getMockMarketData());
        marketDataList.add(getMockMarketData());
        marketDataList.add(getMockMarketData());

        when(marketDataService.findAll()).thenReturn(marketDataList);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get("/v1/market-data/all")
                .accept(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        MockHttpServletResponse response = result.getResponse();

        ObjectMapper mapper = new ObjectMapper();

        List<MarketDataDto> actual = mapper.readValue(response.getContentAsString(), new TypeReference<List<MarketDataDto>>() {
        });

        assertEquals(HttpStatus.OK.value(), response.getStatus());
        assertEquals(3, actual.size());
    }

    @Test
    void findInPaging() throws Exception {
        int page = 0;
        int size = 5;
        List<MarketData> marketDataList = new ArrayList<>();

        marketDataList.add(getMockMarketData());
        marketDataList.add(getMockMarketData());
        marketDataList.add(getMockMarketData());
        marketDataList.add(getMockMarketData());
        marketDataList.add(getMockMarketData());


        when(marketDataService.findInPaging(anyInt(), anyInt())).thenReturn(marketDataList);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get("/v1/market-data")
                .param("page", String.valueOf(page))
                .param("size", String.valueOf(size))
                .accept(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        MockHttpServletResponse response = result.getResponse();

        ObjectMapper mapper = new ObjectMapper();

        List<MarketDataDto> actual = mapper.readValue(response.getContentAsString(), new TypeReference<List<MarketDataDto>>() {
        });

        assertEquals(HttpStatus.OK.value(), response.getStatus());
        assertEquals(size, actual.size());
    }


    private MarketData getMockMarketData() {
        return MarketData.builder()
                .id(new Date().getTime())
                .mins(5)
                .symbol("ETH")
                .price(BigDecimal.valueOf(3232.2323))
                .createTime(new Date())
                .dayQuarter(3).build();
    }

    private MarketDataDto toDto(MarketData data) {
        return MarketDataDto.builder()
                .id(data.getId())
                .createTime(data.getCreateTime())
                .dayQuarter(data.getDayQuarter())
                .mins(data.getMins())
                .price(data.getPrice())
                .symbol(data.getSymbol()).build();
    }
}