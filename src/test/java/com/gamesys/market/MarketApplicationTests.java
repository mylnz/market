package com.gamesys.market;

import com.gamesys.market.controller.MarketDataController;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class MarketApplicationTests {

    @InjectMocks
    private MarketDataController marketDataController;

    @Test
    void contextLoads() {
        assertThat(marketDataController).isNotNull();
    }

}
