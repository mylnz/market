package com.gamesys.market.service.impl;

import com.gamesys.market.model.MarketData;
import com.gamesys.market.repository.MarketDataRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MarketDataServiceImplTest {

    @Mock
    MarketDataRepository marketDataRepository;

    @InjectMocks
    MarketDataServiceImpl marketDataService;

    @Test
    void findById() {
        Long id = new Date().getTime();
        MarketData marketData = MarketData.builder()
                .id(id)
                .mins(5)
                .symbol("ETH")
                .price(BigDecimal.valueOf(3232.2323))
                .createTime(new Date())
                .dayQuarter(3).build();

        when(marketDataService.findById(id)).thenReturn(Optional.of(marketData));

        assertNotNull(marketDataService.findById(id));
    }

    @Test
    void findAll() {
        List<MarketData> marketDataList = new ArrayList<>();
        marketDataList.add(MarketData.builder().build());
        marketDataList.add(MarketData.builder().build());
        marketDataList.add(MarketData.builder().build());

        when(marketDataService.findAll()).thenReturn(marketDataList);

        List<MarketData> result = marketDataService.findAll();
        assertEquals(3, result.size());
        verify(marketDataRepository).findAll();
    }

    @Test
    void findInPaging() {
        List<MarketData> marketDataList = new ArrayList<>();
        marketDataList.add(MarketData.builder().build());
        marketDataList.add(MarketData.builder().build());
        marketDataList.add(MarketData.builder().build());

        when(marketDataService.findInPaging(0, 10)).thenReturn(marketDataList);

        List<MarketData> result = marketDataService.findInPaging(0, 10);
        assertEquals(3, result.size());
        verify(marketDataRepository).findInPaging(0, 10);
    }

    @Test
    void saveMarketData() {

        Long id = new Date().getTime();
        MarketData marketData = MarketData.builder()
                .id(id)
                .mins(5)
                .symbol("ETH")
                .price(BigDecimal.valueOf(3232.2323))
                .createTime(new Date())
                .dayQuarter(3).build();

        when(marketDataService.saveMarketData(marketData)).thenReturn(id);

        assertEquals(marketDataService.saveMarketData(marketData), marketData.getId());
    }
}