package com.gamesys.market.repository.impl;

import com.gamesys.market.model.MarketData;
import com.gamesys.market.repository.MarketDataRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.jdbc.Sql;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@JdbcTest
@Sql({"classpath:schema.sql", "classpath:data.sql"})
class MarketDataRepositoryImplTest {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private MarketDataRepository repository;

    @BeforeEach
    void setUp() {
        repository = new MarketDataRepositoryImpl(jdbcTemplate);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void whenInitDataSourceShouldReturnCorrectDataCount() {
        assertEquals(3, repository.findAll().size());
    }

    @Test
    void whenSaveToDataSourceShouldReturnId() {
        MarketData marketData = MarketData.builder()
                .id(new Date().getTime())
                .mins(5)
                .symbol("ETH")
                .price(BigDecimal.valueOf(3232.2323))
                .createTime(new Date())
                .dayQuarter(3).build();

        assertEquals(marketData.getId(), repository.save(marketData));

    }

    @Test
    void whenFindByIdShouldReturnTestData() {
        Long testId = 19695L;
        Optional<MarketData> optional = repository.findById(testId);

        assertTrue(optional.isPresent());
        assertEquals(testId, optional.get().getId());
        assertNotNull(optional.get().getPrice());
        assertNotNull(optional.get().getSymbol());
        assertEquals("MYTH", optional.get().getSymbol());
    }

    @Test
    void whenFindInPagingShouldReturnCorrectPageAndSize() {
        List<MarketData> data = repository.findInPaging(0,1);
        assertEquals(1, data.size());
        assertEquals( 19695L, data.get(0).getId());

        data = repository.findInPaging(1,1);
        assertEquals(1, data.size());
        assertEquals( 265566L, data.get(0).getId());

        data = repository.findInPaging(0,5);
        assertEquals(3, data.size());
    }
}