package com.gamesys.market.consumer;

import com.gamesys.market.config.ScheduleConfig;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import static org.assertj.core.api.Assertions.assertThat;

@SpringJUnitConfig(ScheduleConfig.class)
class MarketDataCollectorIntegrationTest {

    @Autowired
    private MarketDataCollector dataCollector;

    @Test
    void whenAwaitFiveSecondShouldInvocationCountIsGreaterThanZero()throws InterruptedException {
        Thread.sleep(5000);

        assertThat(dataCollector.getInvocationCount()).isPositive();
    }
}