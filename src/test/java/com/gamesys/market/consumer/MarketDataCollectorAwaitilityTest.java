package com.gamesys.market.consumer;

import com.gamesys.market.config.ScheduleConfig;
import org.awaitility.Duration;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import static org.awaitility.Awaitility.await;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.verify;

@SpringJUnitConfig(ScheduleConfig.class)
class MarketDataCollectorAwaitilityTest {

    @SpyBean
    private MarketDataCollector dataCollector;

    @Test
    void whenWaitFiveSecondShouldScheduledIsCalledAtLeastFiveTimes() {
        await()
                .atMost(Duration.FIVE_SECONDS)
                .untilAsserted(() -> verify(dataCollector, atLeast(5)).collectMarketData());
    }
}