DROP TABLE IF EXISTS market_data;

CREATE TABLE market_data(
  id LONG PRIMARY KEY,
  symbol VARCHAR(12) NOT NULL,
  price DECIMAL(12,4) NOT NULL,
  mins integer not null,
  createTime DATETIME DEFAULT now(),
  dayQuarter integer default 0
);
