package com.gamesys.market.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.RepresentationModel;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class MarketDataDto extends RepresentationModel<MarketDataDto> implements Serializable {
    private Long id;
    private String symbol;
    private Date createTime;
    private BigDecimal price;
    private Integer mins;
    private Integer dayQuarter;
}
