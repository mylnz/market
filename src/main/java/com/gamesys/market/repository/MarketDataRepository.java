package com.gamesys.market.repository;

import com.gamesys.market.model.MarketData;

import java.util.List;
import java.util.Optional;

public interface MarketDataRepository {
    Long save(MarketData marketData);

    long simpleSave(MarketData marketData);

    Optional<MarketData> findById(long id);

    List<MarketData> findAll();

    List<MarketData> findInPaging(int page, int size);
}
