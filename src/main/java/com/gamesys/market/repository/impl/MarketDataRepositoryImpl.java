package com.gamesys.market.repository.impl;

import com.gamesys.market.mapper.MarketDataRowMapper;
import com.gamesys.market.model.MarketData;
import com.gamesys.market.repository.MarketDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.util.List;
import java.util.Optional;

@Repository
public class MarketDataRepositoryImpl implements MarketDataRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public MarketDataRepositoryImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Long save(MarketData marketData) {
        String insertSql = "INSERT INTO `market_data` (id, symbol, price, mins, createTime, dayQuarter)"
                + " VALUES(?,?,?,?,?,?)";

        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(insertSql, new String[]{"id"});

            ps.setLong(1, marketData.getId());
            ps.setString(2, marketData.getSymbol());
            ps.setBigDecimal(3, marketData.getPrice());
            ps.setInt(4, marketData.getMins());
            ps.setDate(5, new java.sql.Date(marketData.getCreateTime().getTime()));
            ps.setInt(6, marketData.getDayQuarter());
            return ps;
        }, keyHolder);

        return keyHolder.getKey().longValue();
    }

    @Override
    public long simpleSave(MarketData marketData) {
        SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplate)
                .withTableName("market_data")
                .usingGeneratedKeyColumns("id");
        return simpleJdbcInsert.executeAndReturnKey(marketData.toMap()).longValue();
    }

    @Override
    public Optional<MarketData> findById(long id) {
        return Optional.of(jdbcTemplate.queryForObject(
                "select * from market_data where id=?",
                new MarketDataRowMapper(),
                new Object[]{id}));
    }

    @Override
    public List<MarketData> findAll() {
        return jdbcTemplate.query("select * from market_data", new MarketDataRowMapper());
    }

    @Override
    public List<MarketData> findInPaging(int page, int size) {
        return jdbcTemplate.query("select * from market_data ORDER BY id LIMIT " + size + " OFFSET " + page * size,
                new MarketDataRowMapper());
    }
}
