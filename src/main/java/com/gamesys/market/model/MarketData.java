package com.gamesys.market.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class MarketData {
    private Long id;
    private String symbol;
    private Date createTime;
    private BigDecimal price;
    private Integer mins;
    private Integer dayQuarter;

    public Map<String, Object> toMap() {
        Map<String, Object> values = new HashMap<>();
        values.put("id", getId());
        values.put("symbol", getSymbol());
        values.put("price", getPrice());
        values.put("mins", getMins());
        values.put("createTime", getCreateTime());
        values.put("dayQuarter", getDayQuarter());
        return values;
    }
}
