package com.gamesys.market.consumer;

import com.gamesys.market.model.MarketData;
import com.gamesys.market.service.MarketDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

@Component
public class MarketDataCollector {
    private static final Logger LOG = LoggerFactory.getLogger(MarketDataCollector.class);
    private static final String SYMBOL_ETH_USD = "ETHUSDT";

    private final MarketDataService marketDataService;

    private URI apiUri;

    private AtomicInteger count = new AtomicInteger(0);

    public MarketDataCollector(MarketDataService marketDataService) {
        this.marketDataService = marketDataService;
        setApiUri();
    }

    private void setApiUri() {
        final String url = "https://api.binance.com/api/v3/avgPrice";

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url)
                .queryParam("symbol", SYMBOL_ETH_USD);

        this.apiUri = builder.build().encode().toUri();
    }

    @Scheduled(fixedRate = 1000)
    public void collectMarketData() {
        RestTemplate restTemplate = new RestTemplate();
        MarketData result = restTemplate.getForObject(this.apiUri, MarketData.class);

        if (result != null) {
            marketDataService.saveMarketData(MarketData.builder()
                    .symbol(SYMBOL_ETH_USD)
                    .id(new Date().getTime())
                    .mins(result.getMins())
                    .price(result.getPrice())
                    .createTime(new Date()).build());
        }

        this.count.incrementAndGet();
        LOG.debug("Record was saved successfully!");
    }

    public int getInvocationCount() {
        return this.count.get();
    }
}
