package com.gamesys.market.controller;

import com.gamesys.market.dto.MarketDataDto;
import com.gamesys.market.model.MarketData;
import com.gamesys.market.service.MarketDataService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/v1/market-data")
public class MarketDataController {

    @Autowired
    MarketDataService marketDataService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("/{id}")
    public ResponseEntity<MarketDataDto> findById(@PathVariable("id") long id) {

        Optional<MarketData> optional = marketDataService.findById(id);
        if (optional.isPresent()) {
            return ResponseEntity.ok(toDto(optional.get()));
        }

        return ResponseEntity.noContent().build();
    }

    @GetMapping("/all")
    public ResponseEntity<List<MarketDataDto>> findAll() {
        List<MarketData> marketData = marketDataService.findAll();

        if (marketData.isEmpty()) {
            return ResponseEntity.noContent().build();
        }

        return ResponseEntity.ok().body(marketData.stream()
                .map(this::toDto)
                .collect(Collectors.toList()));
    }

    @GetMapping()
    public ResponseEntity<List<MarketDataDto>> findInPaging(@RequestParam int page, @RequestParam int size) {
        List<MarketData> marketData = marketDataService.findInPaging(page, size);

        if (marketData.isEmpty()) {
            return ResponseEntity.noContent().build();
        }

        return ResponseEntity.ok().body(marketData.stream()
                .map(this::toDto)
                .collect(Collectors.toList()));
    }

    private MarketDataDto toDto(MarketData entity) {
        return modelMapper.map(entity, MarketDataDto.class);
    }
}
