package com.gamesys.market.service;

import com.gamesys.market.model.MarketData;

import java.util.List;
import java.util.Optional;

public interface MarketDataService {
    Optional<MarketData> findById(long id);

    List<MarketData> findAll();

    List<MarketData> findInPaging(int page, int size);

    long saveMarketData(MarketData marketData);
}
