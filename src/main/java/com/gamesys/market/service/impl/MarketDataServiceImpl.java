package com.gamesys.market.service.impl;

import com.gamesys.market.model.MarketData;
import com.gamesys.market.repository.MarketDataRepository;
import com.gamesys.market.service.MarketDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class MarketDataServiceImpl implements MarketDataService {

    @Autowired
    private MarketDataRepository marketDataRepository;

    @Override
    public Optional<MarketData> findById(long id) {
        return marketDataRepository.findById(id);
    }

    @Override
    public List<MarketData> findAll() {
        return marketDataRepository.findAll();
    }

    @Override
    public List<MarketData> findInPaging(int page, int size) {
        return marketDataRepository.findInPaging(page, size);
    }


    @Override
    public long saveMarketData(MarketData marketData) {
        marketData.setDayQuarter(getDayQuarter(marketData.getCreateTime()));
        return marketDataRepository.save(marketData);
    }

    private int getDayQuarter(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int hours = calendar.get(Calendar.HOUR_OF_DAY);
        return (int) Math.ceil(hours / 6.0);
    }
}
