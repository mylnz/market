package com.gamesys.market.mapper;

import com.gamesys.market.model.MarketData;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MarketDataRowMapper implements RowMapper<MarketData> {

    @Override
    public MarketData mapRow(ResultSet rs, int rowNum) throws SQLException {
        return MarketData.builder()
                .id(rs.getLong("id"))
                .createTime(rs.getTimestamp("createTime"))
                .price(rs.getBigDecimal("price"))
                .symbol(rs.getString("symbol"))
                .mins(rs.getInt("mins"))
                .dayQuarter(rs.getInt("dayQuarter"))
                .build();
    }
}