# Market Data Application

This is a application to consume market data from remote api and retrieve via endpoints.


## Libraries used

* [Spring Boot](https://spring.io/projects/spring-boot)
* [Maven](https://maven.apache.org/index.html)
* [H2 Database](https://www.h2database.com/html/main.html)
* [Junit5](https://junit.org/junit5/)
* [Lombok](https://projectlombok.org/)
* [Hal Explorer]()
* [JDBC](https://spring.io/projects/spring-data-jdbc)
* [Awaitility](https://github.com/awaitility/awaitility)
* [Docker](https://www.docker.com/)


## Features

* Consume market data at every 1 second from  [binance.api](https://api.binance.com/).
* Set the quarter of the day to market data
* Save to [market_data] table on [H2 Database](https://www.h2database.com/html/main.html)  
* Retrieve all saved data from the database.
* Retrieve a record by record id
* Retrieve data by paging


##  Installation and Getting Started

Clone the repository

```bash
git clone https://mylnz@bitbucket.org/mylnz/market.git
```

Install dependecies with maven

```bash
mvn clean install
```

```bash
mvn spring-boot:run
```

* Browse http://localhost:8080/

## Start with Docker

```bash
docker pull mylnz/market:latest
```

```bash
docker run -e "SPRING_PROFILES_ACTIVE=development" -p 8080:8080 -d mylnz/market
```

* Browse http://localhost:8080/

## Versions
* 0.0.1-SNAPSHOT 

## Developers
* Mustafa Yalniz

## License
Open Source software released under the Apache 2.0 license.